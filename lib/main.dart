import 'package:flutter/material.dart';

void main() {
    runApp( const MyApp() );
}

class MyApp extends StatelessWidget {
    const MyApp({Key? key}) : super(key: key);

    @override
        Widget build(BuildContext context) {

            return MaterialApp(
                    home: Scaffold(
                        appBar: AppBar(
                            backgroundColor: Colors.blue,
                            title: const Text("I made this app in Neovim !!"),
                            ),
                        body: const MyInside()
                        ),
                    );

        }
}

class MyInside extends StatelessWidget {
    const MyInside({Key? key}) : super(key: key);

    @override
        Widget build(BuildContext context) {
            return Row(
                    children: [
                    Column(
                        children: const [
                        Text("I've made some changes")
                        ],
                        ),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                            Column(
                                children: [
                                Text("This is my Flutter App"),
                                Row(
                                    children: const [
                                    Icon(Icons.home),
                                    Icon(Icons.key),
                                    ],
                                   ),
                                ],
                                )
                            ],
                           )
                        ],
                        ),
                        Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: const [
                                Text("Left Column")
                                ],
                              ),
                        ],
                        );
        }
}
